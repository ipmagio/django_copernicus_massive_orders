## Deprecation notice

> This project is deprecated. It has been replaced by the newer 
> `giosystem-events` project, which is available on gitlab.com at
>
> https://gitlab.com/giosystem/giosystem-events


This is a django app to record the massive orders of Copernicus.
