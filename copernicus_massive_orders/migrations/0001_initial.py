# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Massive_Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order_name', models.TextField(blank=True)),
                ('date', models.DateField(null=True, blank=True)),
                ('username', models.TextField(blank=True)),
                ('company', models.TextField(blank=True)),
                ('country', models.TextField(blank=True)),
                ('domain_of_activity', models.TextField(blank=True)),
                ('type_of_organization', models.TextField(blank=True)),
                ('variables', models.CharField(blank=True, max_length=30, choices=[(b'LST', b'LST'), (b'LST10', b'LST10'), (b'LST10S', b'LST10S')])),
                ('volume_of_downloaded_data_GB', models.FloatField(blank=True)),
                ('number_of_products', models.IntegerField(blank=True)),
            ],
        ),
    ]
