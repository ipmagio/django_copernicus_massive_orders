from django.contrib import admin
from django.contrib.admin import register

import models

@register(models.Massive_Order)
class MassiveOrderAdmin(admin.ModelAdmin):
    list_display = ('order_name', 'date', 'username', 'company', 'country',
                  'domain_of_activity', 'type_of_organization', 'variables',
                  'volume_of_downloaded_data_GB', 'number_of_products')
