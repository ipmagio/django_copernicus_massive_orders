from django.db import models
from django.utils.encoding import python_2_unicode_compatible

@python_2_unicode_compatible
class Massive_Order(models.Model):

    order_name = models.TextField(blank=True)
    date = models.DateField(blank=True, null=True)
    username = models.TextField(blank=True)
    company = models.TextField(blank=True)
    country = models.TextField(blank=True)
    domain_of_activity = models.TextField(blank=True)
    type_of_organization = models.TextField(blank=True)
    LST = 'LST'
    LST10 = 'LST10'
    LST10S = 'LST10S'
    variables = models.CharField(blank=True,
                        max_length = 30,
                        choices = ((LST, LST),
                                   (LST10, LST10),
                                   (LST10S, LST10S)
                                  )
                )
    volume_of_downloaded_data_GB = models.FloatField(blank=True)
    number_of_products = models.IntegerField(blank=True)  

    def __str__(self):
        return self.username
