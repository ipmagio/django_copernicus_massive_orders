from django.conf.urls import url
from copernicus_massive_orders import views

urlpatterns = [
    url(r'^orders/$', views.MassiveOrderList.as_view())
]

