from rest_framework import serializers

from copernicus_massive_orders.models import Massive_Order

from rest_framework.response import Response


class MassiveOrderSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Massive_Order
        fields = ('order_name', 'date', 'username', 'company', 'country',
                  'domain_of_activity', 'type_of_organization', 'variables',
                  'volume_of_downloaded_data_GB', 'number_of_products')



