from copernicus_massive_orders.models import Massive_Order
from copernicus_massive_orders.serializers import MassiveOrderSerializer
import django_filters
from rest_framework.views import APIView
import datetime
from rest_framework.response import Response
from rest_framework import generics, filters
        
class MassiveOrderList(generics.ListCreateAPIView):
    queryset = Massive_Order.objects.all()
    serializer_class = MassiveOrderSerializer
