import os
from setuptools import setup, find_packages

README = open(os.path.join(os.path.dirname(__file__), 'README.rst')).read()

os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='copernicus_massive_orders',
    version='0.1',
    packages=find_packages(),
    install_requires=['django', 'django-giosystem-settings'],
    include_package_data=True,
    license='BSD License',
    description='A Django app to record the massive orders of Copernicus.',
    long_description=README,
    url='https://bitbucket.org/ipmagio/django_copernicus_massive_orders.git',
    author='Sara Tomaz',
    author_email='sara.tomaz@ipma.pt',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License', # example license
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
)
